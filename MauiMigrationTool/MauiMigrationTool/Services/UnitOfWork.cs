using MauiMigrationTool.Entities;
using MauiMigrationTool.Services.Interfaces;
using SQLite;

namespace MauiMigrationTool.Services;

public class UnitOfWork : IUnitOfWork
{
    public async Task<SQLiteAsyncConnection> GetConnectionAsync()
    {
        await DatabaseInitializer.Instance;
        return DatabaseInitializer.Connection;
    }

    public async Task<List<Customer>> GetItemsAsync()
    {
        var connection = await GetConnectionAsync();
        return await connection.Table<Customer>().ToListAsync();
    }

    public async Task<Customer> GetItemAsync(Guid id)
    {
        var connection = await GetConnectionAsync();
        return await connection.Table<Customer>().Where(i => i.Id == id).FirstOrDefaultAsync();
    }

    public async Task<int> SaveItemAsync(Customer item)
    {
        var connection = await GetConnectionAsync();
        if (item.Id != Guid.Empty)
        {
            return await connection.UpdateAsync(item);
        }

        return await connection.InsertAsync(item);
    }

    public async Task UpdateItemsAsync(IEnumerable<Customer> items)
    {
        var connection = await GetConnectionAsync();

        await connection.UpdateAllAsync(items);
    }
}