using CommunityToolkit.Mvvm.DependencyInjection;
using MauiMigrationTool.Entities;
using MauiMigrationTool.Services.Interfaces;
using MauiMigrationTool.Utils;
using SQLite;

namespace MauiMigrationTool.Services;

public class DatabaseInitializer
{
    private readonly IDatabaseInfoProvider _databaseInfoProvider;
    public static SQLiteAsyncConnection Connection;

    private DatabaseInitializer(IDatabaseInfoProvider databaseInfoProvider)
    {
        _databaseInfoProvider = databaseInfoProvider;
        Connection = new SQLiteAsyncConnection(_databaseInfoProvider.DatabaseFilePath, _databaseInfoProvider.Flags);

        Console.WriteLine($"DataBase path - {databaseInfoProvider.DatabaseFilePath}");
    }

    public static readonly AsyncLazy<DatabaseInitializer> Instance = new AsyncLazy<DatabaseInitializer>(async () =>
    {
        var instance = new DatabaseInitializer(IPlatformApplication.Current.Services.GetRequiredService<IDatabaseInfoProvider>());
        await Connection.CreateTableAsync<Customer>();
        return instance;
    });
}