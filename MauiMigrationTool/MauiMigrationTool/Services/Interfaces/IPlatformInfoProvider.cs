namespace MauiMigrationTool.Services.Interfaces;

public interface IPlatformInfoProvider
{
    IEnumerable<Type> GetAssembliesForType<T>();
}