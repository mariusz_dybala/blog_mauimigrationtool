using MauiMigrationTool.Entities;
using SQLite;

namespace MauiMigrationTool.Services.Interfaces;

public interface IUnitOfWork
{
    Task<SQLiteAsyncConnection> GetConnectionAsync();
    Task<List<Customer>> GetItemsAsync();
    Task<Customer> GetItemAsync(Guid id);
    Task<int> SaveItemAsync(Customer item);
    Task UpdateItemsAsync(IEnumerable<Customer> items);
}