using MauiMigrationTool.Models;

namespace MauiMigrationTool.Services.Interfaces;

public interface IKeyValueStoreService
{
    void SetValue(string key, string value);
    StoredValue GetValue(string key);
}