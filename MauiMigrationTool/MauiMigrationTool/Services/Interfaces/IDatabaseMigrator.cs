namespace MauiMigrationTool.Services.Interfaces;

public interface IDatabaseMigrator
{
    Task ExecuteMigrationsIfNeededAsync();
}