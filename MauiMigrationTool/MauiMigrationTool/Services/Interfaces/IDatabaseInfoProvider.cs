using SQLite;

namespace MauiMigrationTool.Services.Interfaces;

public interface IDatabaseInfoProvider
{
    string DatabaseFilePath { get;}
    SQLiteOpenFlags Flags { get; }
}