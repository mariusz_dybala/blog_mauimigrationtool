namespace MauiMigrationTool.Services.Interfaces;

public interface IDatabaseMigration
{
    Task ExecuteAsync();
}