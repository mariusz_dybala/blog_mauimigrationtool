using MauiMigrationTool.Models;
using MauiMigrationTool.Services.Interfaces;
using MauiMigrationTool.Utils;

namespace MauiMigrationTool.Services;

public class DatabaseMigrator : IDatabaseMigrator
{
    private const string LastMigrationIdKey = "LastMigrationIdKey";
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPlatformInfoProvider _platformInfoProvider;
    private readonly IKeyValueStoreService _keyValueStoreService;

    public DatabaseMigrator(IUnitOfWork unitOfWork, IPlatformInfoProvider platformInfoProvider,
        IKeyValueStoreService keyValueStoreService)
    {
        _unitOfWork = unitOfWork;
        _platformInfoProvider = platformInfoProvider;
        _keyValueStoreService = keyValueStoreService;
    }

    public async Task ExecuteMigrationsIfNeededAsync()
    {
        var lastMigrationId = 0;
        var newLastMigrationId = 0;
        var lastMigrationValue = _keyValueStoreService.GetValue(LastMigrationIdKey);

        if (lastMigrationValue.ValueExists)
        {
            lastMigrationId = lastMigrationValue.AsInt();
        }

        var migrations = GetDefinedMigrationsOrdered();

        foreach (var migration in migrations.Where(m => m.Key > lastMigrationId))
        {
            var migrationInstance = Activator.CreateInstance(migration.Value, _unitOfWork) as IDatabaseMigration;
            if (migrationInstance is { } migrationInstanceCasted)
            {
                await migrationInstanceCasted.ExecuteAsync();
                newLastMigrationId = migration.Key;
            }
        }

        if (newLastMigrationId > lastMigrationId)
        {
            _keyValueStoreService.SetValue(LastMigrationIdKey, newLastMigrationId.ToString());
        }
    }

    private IEnumerable<KeyValuePair<int, Type>> GetDefinedMigrationsOrdered()
    {
        var assemblies = _platformInfoProvider.GetAssembliesForType<DatabaseMigrator>().Where(type =>
            type.Name.Contains("Migration") &&
            type.GetInterface(nameof(IDatabaseMigration)) != null);

        return assemblies.ToDictionary(x => x.GetCustomAttributes(typeof(MigrationAttribute), false)
            .OfType<MigrationAttribute>()
            .FirstOrDefault()!.Id, x => x)
            .OrderBy(x => x.Key);
    }
}