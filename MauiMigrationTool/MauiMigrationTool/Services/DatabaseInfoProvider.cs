using MauiMigrationTool.Services.Interfaces;
using SQLite;

namespace MauiMigrationTool.Services;

public class DatabaseInfoProvider : IDatabaseInfoProvider
{
    private string _dataBaseFileName = "maindb.db3";

    public SQLiteOpenFlags Flags => SQLiteOpenFlags.ReadWrite |
                                    SQLiteOpenFlags.Create |
                                    SQLiteOpenFlags.SharedCache;

    public string DatabaseFilePath =>
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), _dataBaseFileName);
}