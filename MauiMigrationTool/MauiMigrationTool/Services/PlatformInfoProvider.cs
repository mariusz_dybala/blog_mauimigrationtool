using System.Reflection;
using MauiMigrationTool.Services.Interfaces;

namespace MauiMigrationTool.Services;

public class PlatformInfoProvider : IPlatformInfoProvider
{
    public IEnumerable<Type> GetAssembliesForType<T>()
    {
        var assemblyType = typeof(T).GetTypeInfo().Assembly;
        return assemblyType.GetTypes();
    }
}