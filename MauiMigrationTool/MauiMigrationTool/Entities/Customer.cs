using SQLite;

namespace MauiMigrationTool.Entities;

public class Customer
{
    [AutoIncrement]
    [PrimaryKey]
    public Guid Id { get; set; }
    public string Name { get; set; }
    
    public string LastName { get; set; }
    
    public decimal PurchaseAmount { get; set; }
    
    public CustomerRank CustomerRank { get; set; }
}

public enum CustomerRank
{
    NA,
    Gold,
    Silver,
    Bronze,
}