using CommunityToolkit.Mvvm.ComponentModel;

namespace MauiMigrationTool.ViewModels;

public partial class BaseViewModel : ObservableObject
{
    [ObservableProperty]
    private bool _isBusy;

    public virtual Task OnAppearing() => Task.CompletedTask;
    public virtual Task OnDisappearing() => Task.CompletedTask;
    public virtual Task OnNavigatedTo() => Task.CompletedTask;
    public virtual Task OnNavigatingFrom() => Task.CompletedTask;
    public virtual Task OnNavigatedFrom() => Task.CompletedTask;
}