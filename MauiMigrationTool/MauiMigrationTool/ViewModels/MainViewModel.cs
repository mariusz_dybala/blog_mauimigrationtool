using CommunityToolkit.Mvvm.ComponentModel;
using MauiMigrationTool.Entities;
using MauiMigrationTool.Services.Interfaces;
using MauiMigrationTool.VMs;

namespace MauiMigrationTool.ViewModels;

public partial class MainViewModel : BaseViewModel
{
    private readonly IUnitOfWork _unitOfWork;

    [ObservableProperty] 
    private IList<CustomerVM> _customers;


    public MainViewModel(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public override async Task OnAppearing()
    {
        var customers = await _unitOfWork.GetItemsAsync();
        Customers = customers.Select(x => new CustomerVM(x)).ToList();
    }
}