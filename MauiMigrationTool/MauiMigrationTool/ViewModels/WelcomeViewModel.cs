using CommunityToolkit.Mvvm.ComponentModel;
using MauiMigrationTool.Services.Interfaces;

namespace MauiMigrationTool.ViewModels;

public partial class WelcomeViewModel : BaseViewModel
{
    private readonly IDatabaseMigrator _databaseMigrator;

    [ObservableProperty] private string _message;

    public WelcomeViewModel(IDatabaseMigrator databaseMigrator)
    {
        _databaseMigrator = databaseMigrator;
    }

    public override async Task OnNavigatedTo()
    {
        Message = "Migration in progress...";
        await _databaseMigrator.ExecuteMigrationsIfNeededAsync();
        Message = "Downloading content...";
        await GetContentTask();
        Message = "Syncing content...";
        await ApplySynchronization();

        await Shell.Current.GoToAsync("mainpage");
    }

    private async Task ApplySynchronization()
    {
        // Dummy task to mimic content sync 
        await Task.Delay(2000);
    }

    private async Task GetContentTask()
    {
        // Dummy task to mimic content downloading 
        await Task.Delay(2000);
    }
}