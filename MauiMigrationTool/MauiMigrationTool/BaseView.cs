using MauiMigrationTool.ViewModels;

namespace MauiMigrationTool;

public abstract class BaseView<T> : ContentPage where T : BaseViewModel
{
    public abstract T ViewModel { get; }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        
        ViewModel.OnAppearing();
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();
        
        ViewModel.OnDisappearing();
    }

    protected override void OnNavigatedTo(NavigatedToEventArgs args)
    {
        base.OnNavigatedTo(args);
        
        ViewModel.OnNavigatedTo();
    }

    protected override void OnNavigatingFrom(NavigatingFromEventArgs args)
    {
        base.OnNavigatingFrom(args);
        
        ViewModel.OnNavigatingFrom();
    }
    
    protected override void OnNavigatedFrom(NavigatedFromEventArgs args)
    {
        base.OnNavigatedFrom(args);
        
        ViewModel.OnNavigatedFrom();
    }
}