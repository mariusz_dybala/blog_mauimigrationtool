namespace MauiMigrationTool;

public interface IViewLifecycle
{
    void OnAppearing();
}