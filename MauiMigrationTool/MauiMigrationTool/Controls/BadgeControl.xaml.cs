using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Maui.ApplicationModel;

namespace MauiMigrationTool.Controls;

public partial class BadgeControl : ContentView
{
    public static BindableProperty RankStateProperty = BindableProperty.Create(nameof(RankState), typeof(string),
        typeof(BadgeControl), string.Empty, propertyChanged: OnRankStateChanged);

    public string RankState
    {
        get => (string) GetValue(RankStateProperty);
        set => SetValue(RankStateProperty, value);
    }

    public BadgeControl()
    {
        InitializeComponent();

        VisualStateManager.GoToState(this, "Gold");
    }
    
    private static void OnRankStateChanged(BindableObject bindable, object oldvalue, object newvalue)
    {
        if (newvalue == null || newvalue == oldvalue)
            return;

        var badgeControl = (BadgeControl) bindable;

        VisualStateManager.GoToState(badgeControl, newvalue.ToString());
    }
}