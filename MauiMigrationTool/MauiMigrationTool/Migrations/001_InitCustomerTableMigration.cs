using MauiMigrationTool.Entities;
using MauiMigrationTool.Services.Interfaces;
using MauiMigrationTool.Utils;

namespace MauiMigrationTool.Migrations;

[Migration(1)]
public class InitCustomerTableMigration : IDatabaseMigration
{
    private readonly IUnitOfWork _unitOfWork;
    public InitCustomerTableMigration(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task ExecuteAsync()
    {
        var connection = await _unitOfWork.GetConnectionAsync();

        await connection.DropTableAsync<Customer>();
        await connection.CreateTableAsync<Customer>();

        var cusomerKrakow = new Customer {Name = "Mariusz", LastName = "Dybala", PurchaseAmount = 1000};
        var cusomerNewYork = new Customer {Name = "John", LastName = "Smith", PurchaseAmount = 500};
        var cusomerLondon = new Customer {Name = "Robert", LastName = "Brown", PurchaseAmount = 10};

        await connection.InsertAsync(cusomerKrakow);
        await connection.InsertAsync(cusomerNewYork);
        await connection.InsertAsync(cusomerLondon);

        await connection.CloseAsync();

    }
}