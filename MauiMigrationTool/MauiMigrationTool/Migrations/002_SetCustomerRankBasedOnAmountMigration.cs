using MauiMigrationTool.Entities;
using MauiMigrationTool.Services.Interfaces;
using MauiMigrationTool.Utils;

namespace MauiMigrationTool.Migrations;

[Migration(2)]
public class SetCustomerRankBasedOnAmountMigration : IDatabaseMigration
{
    private readonly IUnitOfWork _unitOfWork;
    public SetCustomerRankBasedOnAmountMigration(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task ExecuteAsync()
    {
        //TODO - remove it only for sake of showing the progress on migration
        await Task.Delay(2000);

        var allCustomers = await _unitOfWork.GetItemsAsync();
        foreach (var customer in allCustomers)
        {
            customer.CustomerRank = SetRankOnCustomerBasedOnPurchaseAmount(customer.PurchaseAmount);
        }

        await _unitOfWork.UpdateItemsAsync(allCustomers);
    }

    private CustomerRank SetRankOnCustomerBasedOnPurchaseAmount(decimal amount)
    {
        if (amount >= 0 && amount <= 10)
            return CustomerRank.Bronze;
        if (amount > 10 && amount <= 500)
            return CustomerRank.Silver;
        return CustomerRank.Gold;
    }       
}