using MauiMigrationTool.ViewModels;

namespace MauiMigrationTool;

public partial class WelcomePage
{
    public WelcomePage(WelcomeViewModel vm)
    {
        InitializeComponent();

        BindingContext = vm;
    }
    public override WelcomeViewModel ViewModel => (WelcomeViewModel)BindingContext;
}