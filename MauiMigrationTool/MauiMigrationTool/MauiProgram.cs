﻿using CommunityToolkit.Maui;
using CommunityToolkit.Mvvm.DependencyInjection;
using MauiMigrationTool.Platform;
using MauiMigrationTool.Services;
using MauiMigrationTool.Services.Interfaces;
using MauiMigrationTool.ViewModels;
using Microsoft.Extensions.Logging;

namespace MauiMigrationTool;

public static class MauiProgram
{
    public static MauiApp CreateMauiApp()
    {
        var builder = MauiApp.CreateBuilder();
        builder
            .UseMauiApp<App>()
            .UseMauiCommunityToolkit()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                fonts.AddFont("Roboto-Black.ttf", "RobotoBlack");
                fonts.AddFont("Roboto-Bold.ttf", "RobotoBold");
                fonts.AddFont("Roboto-Medium.ttf", "RobotoMedium");
                fonts.AddFont("Roboto-Regular.ttf", "Roboto");
                fonts.AddFont("Roboto-Light.ttf", "RobotoLight");
                fonts.AddFont("Roboto-Italic.ttf", "RobotoItalic");
            })
            .Services
            .AddSingleton<IKeyValueStoreService, KeyValueStoreService>()
            .AddSingleton<IPlatformInfoProvider, PlatformInfoProvider>()
            .AddSingleton<IDatabaseInfoProvider, DatabaseInfoProvider>()
            .AddSingleton<IDatabaseMigrator, DatabaseMigrator>()
            .AddSingleton<IUnitOfWork, UnitOfWork>()
            .AddSingleton<MainViewModel>()
            .AddSingleton<WelcomeViewModel>()
            .AddSingleton<MainPage>()
            .AddSingleton<WelcomePage>();


#if DEBUG
        builder.Logging.AddDebug();
#endif

        return builder.Build();
    }
}