using Foundation;
using MauiMigrationTool.Models;
using MauiMigrationTool.Services.Interfaces;

namespace MauiMigrationTool.Platform;

public partial class KeyValueStoreService : IKeyValueStoreService
{
    public StoredValue GetValue(string key)
    {
        var value = NSUserDefaults.StandardUserDefaults.StringForKey(key);

        if (string.IsNullOrWhiteSpace(value))
        {
            return new StoredValue(false, default); 
        }

        return new StoredValue(true, value);
    }

    public void SetValue(string key, string value)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            return;
        }

        NSUserDefaults.StandardUserDefaults.SetString(value, key);
        NSUserDefaults.StandardUserDefaults.Synchronize();

        var saved = GetValue(key);
    }
}