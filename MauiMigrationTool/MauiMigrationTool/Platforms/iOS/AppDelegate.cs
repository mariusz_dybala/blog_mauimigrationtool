﻿using CommunityToolkit.Mvvm.DependencyInjection;
using Foundation;
using MauiMigrationTool.Models;
using MauiMigrationTool.Services.Interfaces;
using UIKit;

namespace MauiMigrationTool;

[Register("AppDelegate")]
public class AppDelegate : MauiUIApplicationDelegate
{
    protected override MauiApp CreateMauiApp() => MauiProgram.CreateMauiApp();

}