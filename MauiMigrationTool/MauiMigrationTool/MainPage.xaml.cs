﻿using CommunityToolkit.Mvvm.DependencyInjection;
using MauiMigrationTool.ViewModels;

namespace MauiMigrationTool;

public partial class MainPage
{
    public MainPage(MainViewModel vm)
    {
        InitializeComponent();

        BindingContext = vm;
    }

    public override MainViewModel ViewModel => (MainViewModel)BindingContext;
}