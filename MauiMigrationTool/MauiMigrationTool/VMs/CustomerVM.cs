using CommunityToolkit.Mvvm.ComponentModel;
using MauiMigrationTool.Entities;

namespace MauiMigrationTool.VMs;

public partial class CustomerVM : ObservableObject
{
    [ObservableProperty]
    private string _name;
    
    [ObservableProperty]
    private string _lastName;
    
    [ObservableProperty]
    private decimal _purchaseAmount;
    
    [ObservableProperty]
    private CustomerRank _customerRank;

    public CustomerVM(Customer customer)
    {
        Name = customer.Name;
        LastName = customer.LastName;
        PurchaseAmount = customer.PurchaseAmount;
        CustomerRank = customer.CustomerRank;
    }
}