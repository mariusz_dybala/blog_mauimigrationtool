using System.Globalization;
using System.Runtime.InteropServices;
using MauiMigrationTool.Entities;

namespace MauiMigrationTool.Converters;

public class CustomerRankToVisibilityConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value == null)
            return false;

        if (Enum.TryParse(value.ToString(), out CustomerRank rank))
        {
            return rank != CustomerRank.NA;
        }

        return false;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        return default;
    }
}
