namespace MauiMigrationTool.Utils;

[AttributeUsage(AttributeTargets.Class)]
public class MigrationAttribute : Attribute
{
    public int Id { get; set; }

    public MigrationAttribute(int migrationId)
    {
        Id = migrationId;
    }
}