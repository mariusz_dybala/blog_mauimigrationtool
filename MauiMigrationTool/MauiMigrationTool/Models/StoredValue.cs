namespace MauiMigrationTool.Models;

public class StoredValue
{
    public bool ValueExists { get; set; }
    public string Value { get; set; }

    public StoredValue(bool valueExists, string value)
    {
        ValueExists = valueExists;
        Value = value;
    }
}

public static class StoredValueExtensions
{
    public static int AsInt(this StoredValue storedValue)
    {
        if (!storedValue.ValueExists)
            return default;
        
        return int.TryParse(storedValue.Value, out int intValue) ? intValue : default;
    }
}