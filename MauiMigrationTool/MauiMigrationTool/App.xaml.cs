﻿using MauiMigrationTool.Models;
using MauiMigrationTool.Services.Interfaces;

namespace MauiMigrationTool;

public partial class App : Application
{
    public App()
    {
        InitializeComponent();

        MainPage = new AppShell();
    }
}